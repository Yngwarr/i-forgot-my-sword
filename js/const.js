const BASE_LAYER = 'base';
const WALLS_LAYER = 'walls';

const FPS = 15;

// the silliest 1 a. m. hack
const WALL_OBJ = ['wall'];
const TILE_WALL = 2;

const TILE_SIZE = 32;
const MAP_WIDTH = 20;
const MAP_HEIGHT = 20;

const STEP_COOLDOWN = 10;
const MINOTAUR_COOLDOWN = 5;
const BUTTON_COOLDOWN = 120;

const MINOTAUR_CHARGE = 30;
const MINOTAUR_STUN = 60;

const MINOTAUR_SIGHT = 4;

const DIRECTIONS = {
    n: { x: 0, y: -1 },
    s: { x: 0, y: 1 },
    w: { x: -1, y: 0 },
    e: { x: 1, y: 0 }
};
