function mk_enemy(en, sight, enemies) {
    en.state = 'IDLE';
    en.sight = sight;
    en.dir = null;
    en.timer = 0;
    en.cooldown = 0;
    en.kill = function () {
        if (this.state === 'DIE') return;
        this.timer = 60;
        this.state = 'DIE';
    };
    enemies.push(en);
}

function mk_trap(tr, traps) {
    tr.playAnimation('spikes_DOWN');
    tr.active = false;
    tr.activate = function () {
        tr.playAnimation('spikes_UP');
        this.active = true;
        this.color = '#000';
    };
    tr.deactivate = function () {
        tr.playAnimation('spikes_DOWN');
        this.active = false;
        this.color = '#aaa';
    };
    traps.push(tr);
}

function mk_button(btn, buttons, ts) {
    btn.playAnimation('button_UP');
    btn.traps = ts;
    btn.cooldown = 0;
    btn.press = function () {
        btn.playAnimation('button_DOWN');
        this.cooldown = BUTTON_COOLDOWN;
        this.traps.forEach(tr => tr.activate());
    }
    btn._update = function () {
        this.update();
        if (this.cooldown === 0) return;
        this.cooldown--;
        if (this.cooldown === 0) {
            btn.playAnimation('button_UP');
            this.traps.forEach(tr => tr.deactivate());
        }
    }
    buttons.push(btn);
}

const enemy_update = {
    minotaur: function (player, enemies, ...args) {
        this.update();
        //console.log(this.state);
        switch (this.state) {
            case 'IDLE':
                this.playAnimation('minotaur_IDLE');
                if ((this.dir = spot(player, this, this.sight))) {
                    this.timer = MINOTAUR_CHARGE;
                    this.state = 'CHARGE';
                    break;
                }
                break;
            case 'CHARGE':
                this.playAnimation(`minotaur_CHARGE_${this.dir}`);
                if (this.timer-- === 0) {
                    this.state = 'ENGAGE';
                    break;
                }
                break;
            case 'ENGAGE':
                this.playAnimation(`minotaur_ENGAGE_${this.dir}`);
                if (this.cooldown > 0) { --this.cooldown; break; }
                this.cooldown = MINOTAUR_COOLDOWN;
                const obstacle = ray(this, DIRECTIONS[this.dir], 1, traps);
                if (obstacle === WALL_OBJ) {
                    this.timer = MINOTAUR_STUN;
                    this.state = 'STUN';
                    /* TODO destroy the wall */
                    break;
                }
                if (obstacle && typeof obstacle.kill === 'function') {
                    obstacle.kill();
                }
                v_add(this, DIRECTIONS[this.dir], TILE_SIZE);
                break;
            case 'STUN':
                play_anim(this, `minotaur_STUN_${this.dir}`);
                if (this.timer-- === 0) {
                    this.dir = null;
                    this.state = 'IDLE';
                    break;
                }
                break;
            case 'DIE':
                play_anim(this, 'minotaur_DIE');
                if (this.timer-- === 0) {
                    tileEngine.removeObject(this);
                    rm(this, enemies);
                }
                break;
        }
        if (this.state !== 'DIE' && this.collidesWith(player)) {
            player.kill();
        }
    }
}
