/* ======== IMPORTS ======== */
let { Sprite, GameLoop, TileEngine, Vector, SpriteSheet } = kontra;
let { bindKeys, keyPressed, setImagePath, imageAssets } = kontra;
let { getStoreItem, setStoreItem } = kontra;

/* ======== SYSTEM VARIABLES ======== */
let canvas;
let loop;
let ui;
let sound;
let tileEngine;
let sheet;
// TODO make a loading bar
let ready = false;
let current_level = 0;
let levels_done;

/* ======== GAME OBJECTS ======== */
let player;
let enemies = [];
let traps = [];
let buttons = [];

/* ======== GAME FUNCTIONS ======== */

function spot(what, pos, range) {
    for (let d in DIRECTIONS) {
        if (what === ray(pos, DIRECTIONS[d], range)) {
            return d;
        }
    }
    return null;
}

// doesn't take anchor into account
// Vector -> Vector -> Int
function ray(from, dir, range) {
    let p = v_add(Vector(from.x, from.y), dir, TILE_SIZE);
    let wall, ttl = range;
    let os = tileEngine.getObjects();
    while (ttl > 0
        && (wall = tileEngine.tileAtLayer(WALLS_LAYER, p)) !== undefined) {
        if (wall !== 0) return WALL_OBJ;
        for (let i = 0; i < os.length; ++i) {
            if (traps.includes(os[i]) || buttons.includes(os[i])) continue;
            if (p.x >= os[i].x && p.x <= os[i].x + os[i].width
                && p.y >= os[i].y && p.y <= os[i].y + os[i].height) {
                return os[i];
            }
        }
        v_add(p, dir, TILE_SIZE);
        --ttl;
    }
    return null;
}

// all coordinates are row and column numbers
let levels = [{
    player: vec(2,2),
    minotaurs: [
        vec(11,3),
        vec(12,5),
        vec(6,5),
        vec(2,13),
        vec(17,13),
        vec(1,15),
        vec(17,16),
    ],
    traps: [
        [vec(9,1), [vec(8,1), vec(7,1), vec(6,1)]],
        [vec(2,7), [vec(1,5), vec(2,5), vec(3,5)]],
        [vec(18,7), [vec(17,7), vec(17,8)]],
        [vec(10,13), [vec(10,12), vec(9,12)]],
        [vec(17,13), [vec(18,13), vec(18,12)]],
        [vec(8,17), [vec(6,15), vec(7,15), vec(8,15)]],
    ]
}, {
    player: vec(2,3),
    minotaurs: [
        vec(11,3),
        vec(5,5),
        vec(13,8),
        vec(3,13),
        vec(9,14),
        vec(15,14),
        vec(13,18)
    ],
    traps: [
            [vec(1,1), [vec(2,1), vec(3,1)]],
            [vec(1,9), [vec(1,7), vec(1,8), vec(1,10), vec(1,11)]],
            [vec(1,18),[vec(2,18), vec(3,18)]],
            [vec(13,2),[vec(13,3), vec(13,4)]],
            [vec(18,1),[vec(17,1), vec(16,1)]],
            [vec(16,9),[vec(16,10), vec(15,10)]],
            [vec(18,8),[vec(18,9), vec(18,10)]],
            [vec(18,18),[vec(17,18), vec(16,18)]]
    ]
}, {
    player: vec(3,3),
    minotaurs: [
        vec(6,5),
        vec(14,7),
        vec(10,11),
        vec(18,12),
        vec(13,14),
        vec(7,15),
        vec(3,16),
    ],
    traps: [
        [vec(12,1), [vec(10,1), vec(9,1)]],
        [vec(7,18), [vec(9,18), vec(10,18)]],
    ]
}];

function load_level(lvl) {
    player.x = lvl.player.x + 1;
    player.y = lvl.player.y + 1;

    lvl.minotaurs.forEach(v => {
        let m = Sprite({
            x: v.x+1,
            y: v.y+1,
            animations: sheet.animations,
            width: TILE_SIZE - 2,
            height: TILE_SIZE - 2
        });

        mk_enemy(m, MINOTAUR_SIGHT, enemies);
        m._update = enemy_update.minotaur;
    });

    lvl.traps.forEach(([v, vs]) => {
        let ts = [];
        vs.forEach(tv => {
            let tr = Sprite({
                x: tv.x,
                y: tv.y,
                animations: sheet.animations,
                width: TILE_SIZE,
                height: TILE_SIZE
            });
            mk_trap(tr, traps);
            ts.push(tr);
        });
        let b = Sprite({
            x: v.x,
            y: v.y,
            animations: sheet.animations,
            width: TILE_SIZE,
            height: TILE_SIZE
        });
        mk_button(b, buttons, ts);
    });

    tileEngine.setLayer(WALLS_LAYER, lvl.walls);
    tileEngine.setLayer('decor', lvl.decor);
}

function new_game(lvl) {
    ready = false;

    enemies.forEach(o => tileEngine.removeObject(o))
    traps.forEach(o => tileEngine.removeObject(o))
    buttons.forEach(o => tileEngine.removeObject(o))

    // I guess, GC will bite my ass...
    enemies = [];
    traps = [];
    buttons = [];

    load_level(lvl);

    enemies.forEach(en => tileEngine.addObject(en));
    buttons.forEach(btn => tileEngine.addObject(btn));
    traps.forEach(tr => tileEngine.addObject(tr));

    ready = true;
}

function populate_levels() {
    let grid = document.getElementById('levels');
    let line, scr;
    for (let i = 0; i < levels.length; ++i) {
        // the right way is so much verbose
        line = `<img src='img/door${i+1}.svg'>`;
        scr = `ui.hideAll();new_game(levels[${i}]);current_level=${i}`;
        grid.innerHTML += `<button data-n='${i}' onclick='${scr}'>
            ${line}
        </button>`;
    }
}

/* ======== GAME LOOP ======== */
function init() {
    canvas = kontra.init('game').canvas;
    kontra.initKeys();

    sound = new Howl({
        src: ['sfx/knock.wav']
    });

    populate_levels();
    levels_done = getStoreItem('done') || [];

    setImagePath('img/');
    kontra.load(
        /* 0 */ 'tiles.png',
        /* 1 */ 'maps/lvl2.csv',
        /* 2 */ 'sheet.png',
        /* 3 */ 'maps/lvl1.csv',
        /* 4 */ 'maps/lvl3.csv',
        /* 5 */ 'maps/decor3.csv',
        /* 6 */ 'maps/decor1.csv',
        /* 7 */ 'maps/decor2.csv',
    ).then(assets => {
        levels[0].walls = csv2map(assets[1]);
        levels[1].walls = csv2map(assets[3]);
        levels[2].walls = csv2map(assets[4]);

        levels[0].decor = csv2map(assets[6]);
        levels[1].decor = csv2map(assets[7]);
        levels[2].decor = csv2map(assets[5]);

        tileEngine = new TileEngine({
            tilewidth: TILE_SIZE,
            tileheight: TILE_SIZE,

            width: MAP_WIDTH,
            height: MAP_HEIGHT,

            tilesets: [{
                firstgid: 1,
                image: assets[0]
            }],

            layers: [{
                name: BASE_LAYER,
                data: arr_mul([1], MAP_WIDTH * MAP_HEIGHT)
            }, {
                name: WALLS_LAYER,
                data: []
            }, {
                name: 'decor',
                data: []
            }]
        }); 

        sheet = SpriteSheet({
            image: assets[2],
            frameWidth: TILE_SIZE,
            frameHeight: TILE_SIZE,
            animations: {
                'minotaur_IDLE': {frames: '1..1', frameRate: 1},
                'minotaur_CHARGE_s': {frames: '0..2', frameRate: FPS},
                'minotaur_CHARGE_n': {frames: '15..17', frameRate: FPS},
                'minotaur_CHARGE_w': {frames: '30..32', frameRate: FPS},
                'minotaur_CHARGE_e': {frames: '45..47', frameRate: FPS},
                'minotaur_ENGAGE_w': {frames: '60..62', frameRate: FPS},
                'minotaur_ENGAGE_e': {frames: '75..77', frameRate: FPS},
                'minotaur_ENGAGE_s': {frames: '0..2', frameRate: FPS},
                'minotaur_ENGAGE_n': {frames: '15..17', frameRate: FPS},
                'minotaur_STUN_s': {frames: '90..92', frameRate: FPS},
                'minotaur_STUN_n': {frames: '105..107', frameRate: FPS},
                'minotaur_STUN_w': {frames: '93..95', frameRate: FPS},
                'minotaur_STUN_e': {frames: '108..110', frameRate: FPS},
                'minotaur_DIE': {frames: '97..102', frameRate: 6, loop: false},
                'player_IDLE': {frames: '5..5', frameRate: 1},
                'player_MOVE_s': {frames: '4..6', frameRate: FPS},
                'player_MOVE_n': {frames: '19..21', frameRate: FPS},
                'player_MOVE_w': {frames: '34..36', frameRate: FPS},
                'player_MOVE_e': {frames: '49..51', frameRate: FPS},
                'button_UP': { frames: '22..22', frameRate: 1 },
                'button_DOWN': { frames: '23..23', frameRate: 1 },
                'spikes_UP': { frames: '7..7', frameRate: 1 },
                'spikes_DOWN': { frames: '8..8', frameRate: 1 },
            }
        });

        player = Sprite({
            x: 65,
            y: 97,
            animations: sheet.animations,
            width: TILE_SIZE - 2,
            height: TILE_SIZE - 2
        });

        player.kill = function () {
            ui.show('game-over');
        }

        tileEngine.addObject(player);

        new_game(levels[0]);

        ui.show('press-start');
    });

    bindKeys(['left', 'right', 'up', 'down', 'a', 'd', 'q'], e => {
        e.preventDefault();

        if (!ui.shown('stage-select')) return;
        const focused = document.querySelector('button:focus');
        switch (e.key) {
            case 'ArrowLeft':
            case 'a':
            case'q':
                // edge case prevention (text node)
                focused.previousSibling.focus && focused.previousSibling.focus();
                break;
            case 'ArrowRight':
            case 'd':
                // edge case is different (null)
                focused.nextSibling && focused.nextSibling.focus();
                break;
        }
        sound.play();
    });
    bindKeys(['enter', 'space'], e => {
        if (!ui.shown('stage-select')) e.preventDefault();
        if (ui.shown('press-start') || ui.shown('game-over') || ui.shown('victory') || ui.shown('clear')) {
            //ui.hideAll();
            ui.show('stage-select')
            document.querySelector(`#levels [data-n='${current_level}']`).focus();
            sound.play();
        }
    });
    bindKeys(['esc'], e => {
        if (ui.shown('stage-select') || ui.shown('game-over') || ui.shown('victory')) {
            ui.show('press-start');
            sound.play();
        }
        e.preventDefault();
    })

    loop = GameLoop({
        update: update,
        render: render
    });

    ui = new UI(loop);

    loop.start();
}

let step_cooldown = 0;
let last_axis = 'x';
function update(time) {
    if (!ready) return;

    if (enemies.length === 0) {
        if (!levels_done.includes(current_level)) {
            levels_done.push(current_level);
            setStoreItem('done', levels_done);
        }
        if (levels_done.length === 3) {
            ui.show('victory');
        } else {
            ui.show('clear');
        }
    }

    // input handling
    let new_pos = {
        x: player.x,
        y: player.y,
        width: player.width,
        height: player.height
    };
    let d = { x: 0, y: 0 };
    if (step_cooldown === 0) {
        player.playAnimation('player_IDLE');
        if (keyPressed('up') || keyPressed('w') || keyPressed('z')) { d.y -= TILE_SIZE; }
        if (keyPressed('left') || keyPressed('a') || keyPressed('q')) { d.x -= TILE_SIZE; }
        if (keyPressed('down') || keyPressed('s')) { d.y += TILE_SIZE; }
        if (keyPressed('right') || keyPressed('d')) { d.x += TILE_SIZE; }

        if (d.x && d.y) {
            let dx_pos = deepcopy(new_pos);
            let dy_pos = deepcopy(new_pos);
            dx_pos.x += d.x;
            dy_pos.y += d.y;
            if (tileEngine.tileAtLayer(WALLS_LAYER, dx_pos)) {
                d.x = 0;
            } else if (tileEngine.tileAtLayer(WALLS_LAYER, dy_pos)) {
                d.y = 0;
            } else {
                d[last_axis] = 0;
            }
        }
        if (d.x) {
            if (d.x > 0) { player.playAnimation('player_MOVE_e'); }
            else { player.playAnimation('player_MOVE_w'); }
            new_pos.x += d.x;
            last_axis = 'x';
        }
        if (d.y) {
            if (d.y > 0) { player.playAnimation('player_MOVE_s'); }
            else { player.playAnimation('player_MOVE_n'); }
            new_pos.y += d.y;
            last_axis = 'y';
        }

        if (!tileEngine.layerCollidesWith(WALLS_LAYER, new_pos)) {
            player.x = new_pos.x;
            player.y = new_pos.y;
        }

        if (d.x || d.y) step_cooldown = STEP_COOLDOWN;
    } else {
        --step_cooldown;
    }

    // updates
    player.update();
    enemies.forEach(en => en._update(player, enemies));
    buttons.forEach(btn => {
        let pressed = enemies
            .map(en => en.collidesWith(btn))
            .reduce((a, b) => a || b, false);
        if (!pressed && !player.collidesWith(btn)) return;
        btn.press();
    });
    buttons.forEach(btn => btn._update(player, buttons));
    traps.forEach(tr => {
        if (!tr.active) return;

        let dead = enemies.filter(en => en.collidesWith(tr));
        if (player.collidesWith(tr)) dead.push(player);

        dead.forEach(d => {
            d.kill();
        });
    });
}

function render() {
    if (!ready) return;
    tileEngine.render();
    buttons.forEach(btn => btn.render());
    traps.forEach(tr => tr.render());
    player.render();
    enemies.forEach(en => en.render());
}
