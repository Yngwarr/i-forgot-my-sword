function last(lst) {
    return lst[lst.length - 1];
}

function rm(what, list) {
    const idx = list.indexOf(what);
    if (idx < 0) return idx;
    list.splice(idx, 1);
    return idx;
}

function arr_mul(arr, n) {
    let res = [];
    for (let i = 0; i < n; ++i) {
        res = res.concat(arr);
    }
    return res;
}

function deepcopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function csv2map (csv, offset = 0) {
    let mtx = csv
        .split('\n')
        .filter(s => s.length)
        .map(s => s.split(',')
            .map(n => parseInt(n, 10) + offset));
    console.log(`${mtx[0].length}x${mtx.length} level loaded`);
    return mtx.reduce((a, b) => a.concat(b), [])
}

function v_add(to, from, dt = 1) {
    to.x += from.x * dt;
    to.y += from.y * dt;
    return to;
}

function play_anim(sp, name) {
    if (sp.currentAnimation === sp.animations[name]) return;
    sp.playAnimation(name);
}

function vec(x, y) {
    return Vector(x*TILE_SIZE, y*TILE_SIZE);
}
